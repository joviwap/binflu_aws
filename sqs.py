import logging
import boto3
import json
import re

class Sqs(object):
    def __init__(self, queueName, priority):
        self.initLogger()

        self.queueName = queueName
        self.priority = priority
        self.logger.info('Priority is: {}'.format(self.priority))
        self.logger.info('Setting up SQS')
        self.sqs = boto3.client('sqs')
        self.sqsName = self.getQueue(self.queueName, self.priority)
        self.logger.info('Setted up SQS')

    def initLogger(self):
        logging.basicConfig()
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)

    def getQueue(self, queueName, priority):
        if (priority == 'BASE'):
            name = queueName+'.fifo'
        else:
            name = queueName+'_priority_'+variant+'.fifo'
        return self.sqs.get_queue_url(QueueName=name)["QueueUrl"]

    def getMessages(self):
         return self.sqs.receive_message(
            QueueUrl=self.sqsName,
            MaxNumberOfMessages=10,
            AttributeNames=['MessageDeduplicationId']
        )

    def getMessageData(self, message):
        try:
            data = json.loads(message['Body'])
        except Exception:
            data = message['Body']
        return data

    def replanMessage(self, items, id, retryLimit):
        try:
            items = json.dumps(items)
            self.sqs.send_message(
                QueueUrl=self.sqsName,
                MessageBody=items,
                MessageDeduplicationId=self.buildDeduplicationId(id,retryLimit),
                MessageGroupId=self.queueName
            )
            info = "Requeued {} {} task(s) in a single message"
            info = info.format(len(items), self.queueName)
            self.logger.info(info)
        except Exception as err:
            info = "message {} will not be replaned due {}".format(id, err)
            self.logger.info(info)

    def buildDeduplicationId(self, id, limit):
        parts = re.search('^(.*)(-)(\d)$', id)
        if not parts:
            return id+"-1"
        else:
            retrys = int(parts.group(3))
            if(retrys < limit):
                return parts.group(1)+'-'+str(retrys+1)
            else:
                raise Exception('Max limit of retry reached')

    def deleteMessage(self, message):
        self.sqs.delete_message(
            QueueUrl=self.sqsName,
            ReceiptHandle=message['ReceiptHandle']
        )
